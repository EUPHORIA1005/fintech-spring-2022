package ru.tinkoff.fintech.product;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.product.dto.ProductDto;
import ru.tinkoff.fintech.product.mapper.ProductMapper;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    public ProductDto getById(Long id) {
        return findById(id)
                .map(productMapper::fromEntity)
                .orElseThrow(() -> new EntityNotFoundException("Product not found by id=" + id));
    }

    public Page<Product> getByPageable(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @Transactional
    public void update(Product updatedProduct) {
        findById(updatedProduct.getId())
            .ifPresentOrElse(
                (product) -> productRepository.save(updatedProduct),
                () -> {
                    throw new IllegalArgumentException("Can't update product because product with id=" + updatedProduct.getId() + " not exist");
                }
            );
    }

    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

}
